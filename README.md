# thinkphp-c-eval

#### 介绍
thinkphp 内核 C扩展 eval版

#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2019/1209/103836_caf2c972_403172.png "thinkphp.png")


#### 安装教程

1.  git clone 下载并进入目录
2.  phpize
3.  ./configure
4.  make
5.  make install

#### 使用说明

1.  清空目录thinkphp/library/traits和thinkphp/library/think
2.  注释thinkphp/base.php中代码![输入图片说明](https://images.gitee.com/uploads/images/2019/1209/104122_b9b1aa49_403172.png "base.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
