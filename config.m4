dnl $Id$
dnl config.m4 for extension thinkphp

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

dnl PHP_ARG_WITH(thinkphp, for thinkphp support,
dnl Make sure that the comment is aligned:
dnl [  --with-thinkphp             Include thinkphp support])

dnl Otherwise use enable:

PHP_ARG_ENABLE(thinkphp, whether to enable thinkphp support,
Make sure that the comment is aligned:
[  --enable-thinkphp           Enable thinkphp support])

if test "$PHP_THINKPHP" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-thinkphp -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/thinkphp.h"  # you most likely want to change this
  dnl if test -r $PHP_THINKPHP/$SEARCH_FOR; then # path given as parameter
  dnl   THINKPHP_DIR=$PHP_THINKPHP
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for thinkphp files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       THINKPHP_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$THINKPHP_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the thinkphp distribution])
  dnl fi

  dnl # --with-thinkphp -> add include path
  dnl PHP_ADD_INCLUDE($THINKPHP_DIR/include)

  dnl # --with-thinkphp -> check for lib and symbol presence
  dnl LIBNAME=thinkphp # you may want to change this
  dnl LIBSYMBOL=thinkphp # you most likely want to change this

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $THINKPHP_DIR/$PHP_LIBDIR, THINKPHP_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_THINKPHPLIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong thinkphp lib version or lib not found])
  dnl ],[
  dnl   -L$THINKPHP_DIR/$PHP_LIBDIR -lm
  dnl ])
  dnl
  dnl PHP_SUBST(THINKPHP_SHARED_LIBADD)

  PHP_NEW_EXTENSION(thinkphp, thinkphp.c, $ext_shared,, -DZEND_ENABLE_STATIC_TSRMLS_CACHE=1)
fi
