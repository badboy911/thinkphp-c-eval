#ifndef THINK_THINK_DB_BUILDER_SQLSRV_H
#define THINK_THINK_DB_BUILDER_SQLSRV_H

static char* think_db_builder_sqlsrv =
	"\n"
	"// +----------------------------------------------------------------------\n"
	"// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]\n"
	"// +----------------------------------------------------------------------\n"
	"// | Copyright (c) 2006-2012 http://thinkphp.cn All rights reserved.\n"
	"// +----------------------------------------------------------------------\n"
	"// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )\n"
	"// +----------------------------------------------------------------------\n"
	"// | Author: liu21st <liu21st@gmail.com>\n"
	"// +----------------------------------------------------------------------\n"
	"\n"
	"namespace think\\db\\builder;\n"
	"\n"
	"use think\\db\\Builder;\n"
	"use think\\db\\Expression;\n"
	"\n"
	"/**\n"
	" * Sqlsrv数据库驱动\n"
	" */\n"
	"class Sqlsrv extends Builder\n"
	"{\n"
	"    protected $selectSql       = \'SELECT T1.* FROM (SELECT thinkphp.*, ROW_NUMBER() OVER (%ORDER%) AS ROW_NUMBER FROM (SELECT %DISTINCT% %FIELD% FROM %TABLE%%JOIN%%WHERE%%GROUP%%HAVING%) AS thinkphp) AS T1 %LIMIT%%COMMENT%\';\n"
	"    protected $selectInsertSql = \'SELECT %DISTINCT% %FIELD% FROM %TABLE%%JOIN%%WHERE%%GROUP%%HAVING%\';\n"
	"    protected $updateSql       = \'UPDATE %TABLE% SET %SET% FROM %TABLE% %JOIN% %WHERE% %LIMIT% %LOCK%%COMMENT%\';\n"
	"    protected $deleteSql       = \'DELETE FROM %TABLE%  %USING% FROM %TABLE%  %JOIN% %WHERE% %LIMIT% %LOCK%%COMMENT%\';\n"
	"    protected $insertSql       = \'INSERT INTO %TABLE% (%FIELD%) VALUES (%DATA%) %COMMENT%\';\n"
	"    protected $insertAllSql    = \'INSERT INTO %TABLE% (%FIELD%) %DATA% %COMMENT%\';\n"
	"\n"
	"    /**\n"
	"     * order分析\n"
	"     * @access protected\n"
	"     * @param mixed $order\n"
	"     * @param array $options\n"
	"     * @return string\n"
	"     */\n"
	"    protected function parseOrder($order, $options = [])\n"
	"    {\n"
	"        if (empty($order)) {\n"
	"            return \' ORDER BY rand()\';\n"
	"        }\n"
	"\n"
	"        $array = [];\n"
	"        foreach ($order as $key => $val) {\n"
	"            if ($val instanceof Expression) {\n"
	"                $array[] = $val->getValue();\n"
	"            } elseif (is_numeric($key)) {\n"
	"                if (false === strpos($val, \'(\')) {\n"
	"                    $array[] = $this->parseKey($val, $options);\n"
	"                } elseif (\'[rand]\' == $val) {\n"
	"                    $array[] = $this->parseRand();\n"
	"                } else {\n"
	"                    $array[] = $val;\n"
	"                }\n"
	"            } else {\n"
	"                $sort    = in_array(strtolower(trim($val)), [\'asc\', \'desc\'], true) ? \' \' . $val : \'\';\n"
	"                $array[] = $this->parseKey($key, $options, true) . \' \' . $sort;\n"
	"            }\n"
	"        }\n"
	"\n"
	"        return \' ORDER BY \' . implode(\',\', $array);\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 随机排序\n"
	"     * @access protected\n"
	"     * @return string\n"
	"     */\n"
	"    protected function parseRand()\n"
	"    {\n"
	"        return \'rand()\';\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 字段和表名处理\n"
	"     * @access protected\n"
	"     * @param mixed  $key\n"
	"     * @param array  $options\n"
	"     * @return string\n"
	"     */\n"
	"    protected function parseKey($key, $options = [], $strict = false)\n"
	"    {\n"
	"        if (is_numeric($key)) {\n"
	"            return $key;\n"
	"        } elseif ($key instanceof Expression) {\n"
	"            return $key->getValue();\n"
	"        }\n"
	"        $key = trim($key);\n"
	"        if (strpos($key, \'.\') && !preg_match(\'/[,\\\'\\\"\\(\\)\\[\\s]/\', $key)) {\n"
	"            list($table, $key) = explode(\'.\', $key, 2);\n"
	"            if (\'__TABLE__\' == $table) {\n"
	"                $table = $this->query->getTable();\n"
	"            }\n"
	"            if (isset($options[\'alias\'][$table])) {\n"
	"                $table = $options[\'alias\'][$table];\n"
	"            }\n"
	"        }\n"
	"\n"
	"        if ($strict && !preg_match(\'/^[\\w\\.\\*]+$/\', $key)) {\n"
	"            throw new Exception(\'not support data:\' . $key);\n"
	"        }\n"
	"        if (\'*\' != $key && ($strict || !preg_match(\'/[,\\\'\\\"\\*\\(\\)\\[.\\s]/\', $key))) {\n"
	"            $key = \'[\' . $key . \']\';\n"
	"        }\n"
	"        if (isset($table)) {\n"
	"            $key = \'[\' . $table . \'].\' . $key;\n"
	"        }\n"
	"        return $key;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * limit\n"
	"     * @access protected\n"
	"     * @param mixed $limit\n"
	"     * @return string\n"
	"     */\n"
	"    protected function parseLimit($limit)\n"
	"    {\n"
	"        if (empty($limit)) {\n"
	"            return \'\';\n"
	"        }\n"
	"\n"
	"        $limit = explode(\',\', $limit);\n"
	"        if (count($limit) > 1) {\n"
	"            $limitStr = \'(T1.ROW_NUMBER BETWEEN \' . $limit[0] . \' + 1 AND \' . $limit[0] . \' + \' . $limit[1] . \')\';\n"
	"        } else {\n"
	"            $limitStr = \'(T1.ROW_NUMBER BETWEEN 1 AND \' . $limit[0] . \")\";\n"
	"        }\n"
	"        return \'WHERE \' . $limitStr;\n"
	"    }\n"
	"\n"
	"    public function selectInsert($fields, $table, $options)\n"
	"    {\n"
	"        $this->selectSql = $this->selectInsertSql;\n"
	"        return parent::selectInsert($fields, $table, $options);\n"
	"    }\n"
	"\n"
	"}\n"
	"\n"
;
#endif