#ifndef THINK_THINK_CONFIG_DRIVER_JSON_H
#define THINK_THINK_CONFIG_DRIVER_JSON_H

static char* think_config_driver_json =
	"\n"
	"// +----------------------------------------------------------------------\n"
	"// | ThinkPHP [ WE CAN DO IT JUST THINK ]\n"
	"// +----------------------------------------------------------------------\n"
	"// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.\n"
	"// +----------------------------------------------------------------------\n"
	"// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )\n"
	"// +----------------------------------------------------------------------\n"
	"// | Author: liu21st <liu21st@gmail.com>\n"
	"// +----------------------------------------------------------------------\n"
	"\n"
	"namespace think\\config\\driver;\n"
	"\n"
	"class Json\n"
	"{\n"
	"    public function parse($config)\n"
	"    {\n"
	"        if (is_file($config)) {\n"
	"            $config = file_get_contents($config);\n"
	"        }\n"
	"        $result = json_decode($config, true);\n"
	"        return $result;\n"
	"    }\n"
	"}\n"
	"\n"
;
#endif