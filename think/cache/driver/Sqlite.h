#ifndef THINK_THINK_CACHE_DRIVER_SQLITE_H
#define THINK_THINK_CACHE_DRIVER_SQLITE_H

static char* think_cache_driver_sqlite =
	"\n"
	"// +----------------------------------------------------------------------\n"
	"// | ThinkPHP [ WE CAN DO IT JUST THINK ]\n"
	"// +----------------------------------------------------------------------\n"
	"// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.\n"
	"// +----------------------------------------------------------------------\n"
	"// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )\n"
	"// +----------------------------------------------------------------------\n"
	"// | Author: liu21st <liu21st@gmail.com>\n"
	"// +----------------------------------------------------------------------\n"
	"\n"
	"namespace think\\cache\\driver;\n"
	"\n"
	"use think\\cache\\Driver;\n"
	"\n"
	"/**\n"
	" * Sqlite缓存驱动\n"
	" * @author    liu21st <liu21st@gmail.com>\n"
	" */\n"
	"class Sqlite extends Driver\n"
	"{\n"
	"    protected $options = [\n"
	"        \'db\'         => \':memory:\',\n"
	"        \'table\'      => \'sharedmemory\',\n"
	"        \'prefix\'     => \'\',\n"
	"        \'expire\'     => 0,\n"
	"        \'persistent\' => false,\n"
	"    ];\n"
	"\n"
	"    /**\n"
	"     * 构造函数\n"
	"     * @param array $options 缓存参数\n"
	"     * @throws \\BadFunctionCallException\n"
	"     * @access public\n"
	"     */\n"
	"    public function __construct($options = [])\n"
	"    {\n"
	"        if (!extension_loaded(\'sqlite\')) {\n"
	"            throw new \\BadFunctionCallException(\'not support: sqlite\');\n"
	"        }\n"
	"        if (!empty($options)) {\n"
	"            $this->options = array_merge($this->options, $options);\n"
	"        }\n"
	"        $func          = $this->options[\'persistent\'] ? \'sqlite_popen\' : \'sqlite_open\';\n"
	"        $this->handler = $func($this->options[\'db\']);\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 获取实际的缓存标识\n"
	"     * @access public\n"
	"     * @param string $name 缓存名\n"
	"     * @return string\n"
	"     */\n"
	"    protected function getCacheKey($name)\n"
	"    {\n"
	"        return $this->options[\'prefix\'] . sqlite_escape_string($name);\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 判断缓存\n"
	"     * @access public\n"
	"     * @param string $name 缓存变量名\n"
	"     * @return bool\n"
	"     */\n"
	"    public function has($name)\n"
	"    {\n"
	"        $name   = $this->getCacheKey($name);\n"
	"        $sql    = \'SELECT value FROM \' . $this->options[\'table\'] . \' WHERE var=\\\'\' . $name . \'\\\' AND (expire=0 OR expire >\' . $_SERVER[\'REQUEST_TIME\'] . \') LIMIT 1\';\n"
	"        $result = sqlite_query($this->handler, $sql);\n"
	"        return sqlite_num_rows($result);\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 读取缓存\n"
	"     * @access public\n"
	"     * @param string $name 缓存变量名\n"
	"     * @param mixed  $default 默认值\n"
	"     * @return mixed\n"
	"     */\n"
	"    public function get($name, $default = false)\n"
	"    {\n"
	"        $name   = $this->getCacheKey($name);\n"
	"        $sql    = \'SELECT value FROM \' . $this->options[\'table\'] . \' WHERE var=\\\'\' . $name . \'\\\' AND (expire=0 OR expire >\' . $_SERVER[\'REQUEST_TIME\'] . \') LIMIT 1\';\n"
	"        $result = sqlite_query($this->handler, $sql);\n"
	"        if (sqlite_num_rows($result)) {\n"
	"            $content = sqlite_fetch_single($result);\n"
	"            if (function_exists(\'gzcompress\')) {\n"
	"                //启用数据压缩\n"
	"                $content = gzuncompress($content);\n"
	"            }\n"
	"            return unserialize($content);\n"
	"        }\n"
	"        return $default;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 写入缓存\n"
	"     * @access public\n"
	"     * @param string            $name 缓存变量名\n"
	"     * @param mixed             $value  存储数据\n"
	"     * @param integer|\\DateTime $expire  有效时间（秒）\n"
	"     * @return boolean\n"
	"     */\n"
	"    public function set($name, $value, $expire = null)\n"
	"    {\n"
	"        $name  = $this->getCacheKey($name);\n"
	"        $value = sqlite_escape_string(serialize($value));\n"
	"        if (is_null($expire)) {\n"
	"            $expire = $this->options[\'expire\'];\n"
	"        }\n"
	"        if ($expire instanceof \\DateTime) {\n"
	"            $expire = $expire->getTimestamp();\n"
	"        } else {\n"
	"            $expire = (0 == $expire) ? 0 : (time() + $expire); //缓存有效期为0表示永久缓存\n"
	"        }\n"
	"        if (function_exists(\'gzcompress\')) {\n"
	"            //数据压缩\n"
	"            $value = gzcompress($value, 3);\n"
	"        }\n"
	"        if ($this->tag) {\n"
	"            $tag       = $this->tag;\n"
	"            $this->tag = null;\n"
	"        } else {\n"
	"            $tag = \'\';\n"
	"        }\n"
	"        $sql = \'REPLACE INTO \' . $this->options[\'table\'] . \' (var, value, expire, tag) VALUES (\\\'\' . $name . \'\\\', \\\'\' . $value . \'\\\', \\\'\' . $expire . \'\\\', \\\'\' . $tag . \'\\\')\';\n"
	"        if (sqlite_query($this->handler, $sql)) {\n"
	"            return true;\n"
	"        }\n"
	"        return false;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 自增缓存（针对数值缓存）\n"
	"     * @access public\n"
	"     * @param string    $name 缓存变量名\n"
	"     * @param int       $step 步长\n"
	"     * @return false|int\n"
	"     */\n"
	"    public function inc($name, $step = 1)\n"
	"    {\n"
	"        if ($this->has($name)) {\n"
	"            $value = $this->get($name) + $step;\n"
	"        } else {\n"
	"            $value = $step;\n"
	"        }\n"
	"        return $this->set($name, $value, 0) ? $value : false;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 自减缓存（针对数值缓存）\n"
	"     * @access public\n"
	"     * @param string    $name 缓存变量名\n"
	"     * @param int       $step 步长\n"
	"     * @return false|int\n"
	"     */\n"
	"    public function dec($name, $step = 1)\n"
	"    {\n"
	"        if ($this->has($name)) {\n"
	"            $value = $this->get($name) - $step;\n"
	"        } else {\n"
	"            $value = -$step;\n"
	"        }\n"
	"        return $this->set($name, $value, 0) ? $value : false;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 删除缓存\n"
	"     * @access public\n"
	"     * @param string $name 缓存变量名\n"
	"     * @return boolean\n"
	"     */\n"
	"    public function rm($name)\n"
	"    {\n"
	"        $name = $this->getCacheKey($name);\n"
	"        $sql  = \'DELETE FROM \' . $this->options[\'table\'] . \' WHERE var=\\\'\' . $name . \'\\\'\';\n"
	"        sqlite_query($this->handler, $sql);\n"
	"        return true;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 清除缓存\n"
	"     * @access public\n"
	"     * @param string $tag 标签名\n"
	"     * @return boolean\n"
	"     */\n"
	"    public function clear($tag = null)\n"
	"    {\n"
	"        if ($tag) {\n"
	"            $name = sqlite_escape_string($tag);\n"
	"            $sql  = \'DELETE FROM \' . $this->options[\'table\'] . \' WHERE tag=\\\'\' . $name . \'\\\'\';\n"
	"            sqlite_query($this->handler, $sql);\n"
	"            return true;\n"
	"        }\n"
	"        $sql = \'DELETE FROM \' . $this->options[\'table\'];\n"
	"        sqlite_query($this->handler, $sql);\n"
	"        return true;\n"
	"    }\n"
	"}\n"
	"\n"
;
#endif