#ifndef THINK_THINK_CACHE_DRIVER_FILE_H
#define THINK_THINK_CACHE_DRIVER_FILE_H

static char* think_cache_driver_file =
	"\n"
	"// +----------------------------------------------------------------------\n"
	"// | ThinkPHP [ WE CAN DO IT JUST THINK ]\n"
	"// +----------------------------------------------------------------------\n"
	"// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.\n"
	"// +----------------------------------------------------------------------\n"
	"// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )\n"
	"// +----------------------------------------------------------------------\n"
	"// | Author: liu21st <liu21st@gmail.com>\n"
	"// +----------------------------------------------------------------------\n"
	"\n"
	"namespace think\\cache\\driver;\n"
	"\n"
	"use think\\cache\\Driver;\n"
	"\n"
	"/**\n"
	" * 文件类型缓存类\n"
	" * @author    liu21st <liu21st@gmail.com>\n"
	" */\n"
	"class File extends Driver\n"
	"{\n"
	"    protected $options = [\n"
	"        \'expire\'        => 0,\n"
	"        \'cache_subdir\'  => true,\n"
	"        \'prefix\'        => \'\',\n"
	"        \'path\'          => CACHE_PATH,\n"
	"        \'data_compress\' => false,\n"
	"    ];\n"
	"\n"
	"    protected $expire;\n"
	"\n"
	"    /**\n"
	"     * 构造函数\n"
	"     * @param array $options\n"
	"     */\n"
	"    public function __construct($options = [])\n"
	"    {\n"
	"        if (!empty($options)) {\n"
	"            $this->options = array_merge($this->options, $options);\n"
	"        }\n"
	"        if (substr($this->options[\'path\'], -1) != DS) {\n"
	"            $this->options[\'path\'] .= DS;\n"
	"        }\n"
	"        $this->init();\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 初始化检查\n"
	"     * @access private\n"
	"     * @return boolean\n"
	"     */\n"
	"    private function init()\n"
	"    {\n"
	"        // 创建项目缓存目录\n"
	"        if (!is_dir($this->options[\'path\'])) {\n"
	"            if (mkdir($this->options[\'path\'], 0755, true)) {\n"
	"                return true;\n"
	"            }\n"
	"        }\n"
	"        return false;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 取得变量的存储文件名\n"
	"     * @access protected\n"
	"     * @param  string $name 缓存变量名\n"
	"     * @param  bool   $auto 是否自动创建目录\n"
	"     * @return string\n"
	"     */\n"
	"    protected function getCacheKey($name, $auto = false)\n"
	"    {\n"
	"        $name = md5($name);\n"
	"        if ($this->options[\'cache_subdir\']) {\n"
	"            // 使用子目录\n"
	"            $name = substr($name, 0, 2) . DS . substr($name, 2);\n"
	"        }\n"
	"        if ($this->options[\'prefix\']) {\n"
	"            $name = $this->options[\'prefix\'] . DS . $name;\n"
	"        }\n"
	"        $filename = $this->options[\'path\'] . $name . \'.php\';\n"
	"        $dir      = dirname($filename);\n"
	"\n"
	"        if ($auto && !is_dir($dir)) {\n"
	"            mkdir($dir, 0755, true);\n"
	"        }\n"
	"        return $filename;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 判断缓存是否存在\n"
	"     * @access public\n"
	"     * @param string $name 缓存变量名\n"
	"     * @return bool\n"
	"     */\n"
	"    public function has($name)\n"
	"    {\n"
	"        return $this->get($name) ? true : false;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 读取缓存\n"
	"     * @access public\n"
	"     * @param string $name 缓存变量名\n"
	"     * @param mixed  $default 默认值\n"
	"     * @return mixed\n"
	"     */\n"
	"    public function get($name, $default = false)\n"
	"    {\n"
	"        $filename = $this->getCacheKey($name);\n"
	"        if (!is_file($filename)) {\n"
	"            return $default;\n"
	"        }\n"
	"        $content      = file_get_contents($filename);\n"
	"        $this->expire = null;\n"
	"        if (false !== $content) {\n"
	"            $expire = (int) substr($content, 8, 12);\n"
	"            if (0 != $expire && time() > filemtime($filename) + $expire) {\n"
	"                return $default;\n"
	"            }\n"
	"            $this->expire = $expire;\n"
	"            $content      = substr($content, 32);\n"
	"            if ($this->options[\'data_compress\'] && function_exists(\'gzcompress\')) {\n"
	"                //启用数据压缩\n"
	"                $content = gzuncompress($content);\n"
	"            }\n"
	"            $content = unserialize($content);\n"
	"            return $content;\n"
	"        } else {\n"
	"            return $default;\n"
	"        }\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 写入缓存\n"
	"     * @access public\n"
	"     * @param string            $name 缓存变量名\n"
	"     * @param mixed             $value  存储数据\n"
	"     * @param integer|\\DateTime $expire  有效时间（秒）\n"
	"     * @return boolean\n"
	"     */\n"
	"    public function set($name, $value, $expire = null)\n"
	"    {\n"
	"        if (is_null($expire)) {\n"
	"            $expire = $this->options[\'expire\'];\n"
	"        }\n"
	"        if ($expire instanceof \\DateTime) {\n"
	"            $expire = $expire->getTimestamp() - time();\n"
	"        }\n"
	"        $filename = $this->getCacheKey($name, true);\n"
	"        if ($this->tag && !is_file($filename)) {\n"
	"            $first = true;\n"
	"        }\n"
	"        $data = serialize($value);\n"
	"        if ($this->options[\'data_compress\'] && function_exists(\'gzcompress\')) {\n"
	"            //数据压缩\n"
	"            $data = gzcompress($data, 3);\n"
	"        }\n"
	"        $data   = \"<?php\\n//\" . sprintf(\'%012d\', $expire) . \"\\n exit();?>\\n\" . $data;\n"
	"        $result = file_put_contents($filename, $data);\n"
	"        if ($result) {\n"
	"            isset($first) && $this->setTagItem($filename);\n"
	"            clearstatcache();\n"
	"            return true;\n"
	"        } else {\n"
	"            return false;\n"
	"        }\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 自增缓存（针对数值缓存）\n"
	"     * @access public\n"
	"     * @param string    $name 缓存变量名\n"
	"     * @param int       $step 步长\n"
	"     * @return false|int\n"
	"     */\n"
	"    public function inc($name, $step = 1)\n"
	"    {\n"
	"        if ($this->has($name)) {\n"
	"            $value  = $this->get($name) + $step;\n"
	"            $expire = $this->expire;\n"
	"        } else {\n"
	"            $value  = $step;\n"
	"            $expire = 0;\n"
	"        }\n"
	"\n"
	"        return $this->set($name, $value, $expire) ? $value : false;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 自减缓存（针对数值缓存）\n"
	"     * @access public\n"
	"     * @param string    $name 缓存变量名\n"
	"     * @param int       $step 步长\n"
	"     * @return false|int\n"
	"     */\n"
	"    public function dec($name, $step = 1)\n"
	"    {\n"
	"        if ($this->has($name)) {\n"
	"            $value  = $this->get($name) - $step;\n"
	"            $expire = $this->expire;\n"
	"        } else {\n"
	"            $value  = -$step;\n"
	"            $expire = 0;\n"
	"        }\n"
	"\n"
	"        return $this->set($name, $value, $expire) ? $value : false;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 删除缓存\n"
	"     * @access public\n"
	"     * @param string $name 缓存变量名\n"
	"     * @return boolean\n"
	"     */\n"
	"    public function rm($name)\n"
	"    {\n"
	"        $filename = $this->getCacheKey($name);\n"
	"        try {\n"
	"            return $this->unlink($filename);\n"
	"        } catch (\\Exception $e) {\n"
	"        }\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 清除缓存\n"
	"     * @access public\n"
	"     * @param string $tag 标签名\n"
	"     * @return boolean\n"
	"     */\n"
	"    public function clear($tag = null)\n"
	"    {\n"
	"        if ($tag) {\n"
	"            // 指定标签清除\n"
	"            $keys = $this->getTagItem($tag);\n"
	"            foreach ($keys as $key) {\n"
	"                $this->unlink($key);\n"
	"            }\n"
	"            $this->rm(\'tag_\' . md5($tag));\n"
	"            return true;\n"
	"        }\n"
	"        $files = (array) glob($this->options[\'path\'] . ($this->options[\'prefix\'] ? $this->options[\'prefix\'] . DS : \'\') . \'*\');\n"
	"        foreach ($files as $path) {\n"
	"            if (is_dir($path)) {\n"
	"                $matches = glob($path . \'/*.php\');\n"
	"                if (is_array($matches)) {\n"
	"                    array_map(\'unlink\', $matches);\n"
	"                }\n"
	"                rmdir($path);\n"
	"            } else {\n"
	"                unlink($path);\n"
	"            }\n"
	"        }\n"
	"        return true;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 判断文件是否存在后，删除\n"
	"     * @param $path\n"
	"     * @return bool\n"
	"     * @author byron sampson <xiaobo.sun@qq.com>\n"
	"     * @return boolean\n"
	"     */\n"
	"    private function unlink($path)\n"
	"    {\n"
	"        return is_file($path) && unlink($path);\n"
	"    }\n"
	"\n"
	"}\n"
	"\n"
;
#endif