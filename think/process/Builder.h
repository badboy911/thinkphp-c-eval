#ifndef THINK_THINK_PROCESS_BUILDER_H
#define THINK_THINK_PROCESS_BUILDER_H

static char* think_process_builder =
	"\n"
	"// +----------------------------------------------------------------------\n"
	"// | ThinkPHP [ WE CAN DO IT JUST THINK ]\n"
	"// +----------------------------------------------------------------------\n"
	"// | Copyright (c) 2006~2015 http://thinkphp.cn All rights reserved.\n"
	"// +----------------------------------------------------------------------\n"
	"// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )\n"
	"// +----------------------------------------------------------------------\n"
	"// | Author: yunwuxin <448901948@qq.com>\n"
	"// +----------------------------------------------------------------------\n"
	"\n"
	"namespace think\\process;\n"
	"\n"
	"use think\\Process;\n"
	"\n"
	"class Builder\n"
	"{\n"
	"    private $arguments;\n"
	"    private $cwd;\n"
	"    private $env = null;\n"
	"    private $input;\n"
	"    private $timeout        = 60;\n"
	"    private $options        = [];\n"
	"    private $inheritEnv     = true;\n"
	"    private $prefix         = [];\n"
	"    private $outputDisabled = false;\n"
	"\n"
	"    /**\n"
	"     * 构造方法\n"
	"     * @param string[] $arguments 参数\n"
	"     */\n"
	"    public function __construct(array $arguments = [])\n"
	"    {\n"
	"        $this->arguments = $arguments;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 创建一个实例\n"
	"     * @param string[] $arguments 参数\n"
	"     * @return self\n"
	"     */\n"
	"    public static function create(array $arguments = [])\n"
	"    {\n"
	"        return new static($arguments);\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 添加一个参数\n"
	"     * @param string $argument 参数\n"
	"     * @return self\n"
	"     */\n"
	"    public function add($argument)\n"
	"    {\n"
	"        $this->arguments[] = $argument;\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 添加一个前缀\n"
	"     * @param string|array $prefix\n"
	"     * @return self\n"
	"     */\n"
	"    public function setPrefix($prefix)\n"
	"    {\n"
	"        $this->prefix = is_array($prefix) ? $prefix : [$prefix];\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 设置参数\n"
	"     * @param string[] $arguments\n"
	"     * @return  self\n"
	"     */\n"
	"    public function setArguments(array $arguments)\n"
	"    {\n"
	"        $this->arguments = $arguments;\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 设置工作目录\n"
	"     * @param null|string $cwd\n"
	"     * @return  self\n"
	"     */\n"
	"    public function setWorkingDirectory($cwd)\n"
	"    {\n"
	"        $this->cwd = $cwd;\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 是否初始化环境变量\n"
	"     * @param bool $inheritEnv\n"
	"     * @return self\n"
	"     */\n"
	"    public function inheritEnvironmentVariables($inheritEnv = true)\n"
	"    {\n"
	"        $this->inheritEnv = $inheritEnv;\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 设置环境变量\n"
	"     * @param string      $name\n"
	"     * @param null|string $value\n"
	"     * @return self\n"
	"     */\n"
	"    public function setEnv($name, $value)\n"
	"    {\n"
	"        $this->env[$name] = $value;\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     *  添加环境变量\n"
	"     * @param array $variables\n"
	"     * @return self\n"
	"     */\n"
	"    public function addEnvironmentVariables(array $variables)\n"
	"    {\n"
	"        $this->env = array_replace($this->env, $variables);\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 设置输入\n"
	"     * @param mixed $input\n"
	"     * @return self\n"
	"     */\n"
	"    public function setInput($input)\n"
	"    {\n"
	"        $this->input = Utils::validateInput(sprintf(\'%s::%s\', __CLASS__, __FUNCTION__), $input);\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 设置超时时间\n"
	"     * @param float|null $timeout\n"
	"     * @return self\n"
	"     */\n"
	"    public function setTimeout($timeout)\n"
	"    {\n"
	"        if (null === $timeout) {\n"
	"            $this->timeout = null;\n"
	"\n"
	"            return $this;\n"
	"        }\n"
	"\n"
	"        $timeout = (float) $timeout;\n"
	"\n"
	"        if ($timeout < 0) {\n"
	"            throw new \\InvalidArgumentException(\'The timeout value must be a valid positive integer or float number.\');\n"
	"        }\n"
	"\n"
	"        $this->timeout = $timeout;\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 设置proc_open选项\n"
	"     * @param string $name\n"
	"     * @param string $value\n"
	"     * @return self\n"
	"     */\n"
	"    public function setOption($name, $value)\n"
	"    {\n"
	"        $this->options[$name] = $value;\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 禁止输出\n"
	"     * @return self\n"
	"     */\n"
	"    public function disableOutput()\n"
	"    {\n"
	"        $this->outputDisabled = true;\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 开启输出\n"
	"     * @return self\n"
	"     */\n"
	"    public function enableOutput()\n"
	"    {\n"
	"        $this->outputDisabled = false;\n"
	"\n"
	"        return $this;\n"
	"    }\n"
	"\n"
	"    /**\n"
	"     * 创建一个Process实例\n"
	"     * @return Process\n"
	"     */\n"
	"    public function getProcess()\n"
	"    {\n"
	"        if (0 === count($this->prefix) && 0 === count($this->arguments)) {\n"
	"            throw new \\LogicException(\'You must add() command arguments before calling getProcess().\');\n"
	"        }\n"
	"\n"
	"        $options = $this->options;\n"
	"\n"
	"        $arguments = array_merge($this->prefix, $this->arguments);\n"
	"        $script    = implode(\' \', array_map([__NAMESPACE__ . \'\\\\Utils\', \'escapeArgument\'], $arguments));\n"
	"\n"
	"        if ($this->inheritEnv) {\n"
	"            // include $_ENV for BC purposes\n"
	"            $env = array_replace($_ENV, $_SERVER, $this->env);\n"
	"        } else {\n"
	"            $env = $this->env;\n"
	"        }\n"
	"\n"
	"        $process = new Process($script, $this->cwd, $env, $this->input, $this->timeout, $options);\n"
	"\n"
	"        if ($this->outputDisabled) {\n"
	"            $process->disableOutput();\n"
	"        }\n"
	"\n"
	"        return $process;\n"
	"    }\n"
	"}\n"
	"\n"
;
#endif