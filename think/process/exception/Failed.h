#ifndef THINK_THINK_PROCESS_EXCEPTION_FAILED_H
#define THINK_THINK_PROCESS_EXCEPTION_FAILED_H

static char* think_process_exception_failed =
	"\n"
	"// +----------------------------------------------------------------------\n"
	"// | ThinkPHP [ WE CAN DO IT JUST THINK ]\n"
	"// +----------------------------------------------------------------------\n"
	"// | Copyright (c) 2006~2015 http://thinkphp.cn All rights reserved.\n"
	"// +----------------------------------------------------------------------\n"
	"// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )\n"
	"// +----------------------------------------------------------------------\n"
	"// | Author: yunwuxin <448901948@qq.com>\n"
	"// +----------------------------------------------------------------------\n"
	"\n"
	"namespace think\\process\\exception;\n"
	"\n"
	"use think\\Process;\n"
	"\n"
	"class Failed extends \\RuntimeException\n"
	"{\n"
	"\n"
	"    private $process;\n"
	"\n"
	"    public function __construct(Process $process)\n"
	"    {\n"
	"        if ($process->isSuccessful()) {\n"
	"            throw new \\InvalidArgumentException(\'Expected a failed process, but the given process was successful.\');\n"
	"        }\n"
	"\n"
	"        $error = sprintf(\'The command \"%s\" failed.\' . \"\\nExit Code: %s(%s)\", $process->getCommandLine(), $process->getExitCode(), $process->getExitCodeText());\n"
	"\n"
	"        if (!$process->isOutputDisabled()) {\n"
	"            $error .= sprintf(\"\\n\\nOutput:\\n================\\n%s\\n\\nError Output:\\n================\\n%s\", $process->getOutput(), $process->getErrorOutput());\n"
	"        }\n"
	"\n"
	"        parent::__construct($error);\n"
	"\n"
	"        $this->process = $process;\n"
	"    }\n"
	"\n"
	"    public function getProcess()\n"
	"    {\n"
	"        return $this->process;\n"
	"    }\n"
	"}\n"
	"\n"
;
#endif