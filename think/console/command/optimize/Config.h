#ifndef THINK_THINK_CONSOLE_COMMAND_OPTIMIZE_CONFIG_H
#define THINK_THINK_CONSOLE_COMMAND_OPTIMIZE_CONFIG_H

static char* think_console_command_optimize_config =
	"\n"
	"// +----------------------------------------------------------------------\n"
	"// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]\n"
	"// +----------------------------------------------------------------------\n"
	"// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.\n"
	"// +----------------------------------------------------------------------\n"
	"// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )\n"
	"// +----------------------------------------------------------------------\n"
	"// | Author: yunwuxin <448901948@qq.com>\n"
	"// +----------------------------------------------------------------------\n"
	"namespace think\\console\\command\\optimize;\n"
	"\n"
	"use think\\Config as ThinkConfig;\n"
	"use think\\console\\Command;\n"
	"use think\\console\\Input;\n"
	"use think\\console\\input\\Argument;\n"
	"use think\\console\\Output;\n"
	"\n"
	"class Config extends Command\n"
	"{\n"
	"    /** @var  Output */\n"
	"    protected $output;\n"
	"\n"
	"    protected function configure()\n"
	"    {\n"
	"        $this->setName(\'optimize:config\')\n"
	"            ->addArgument(\'module\', Argument::OPTIONAL, \'Build module config cache .\')\n"
	"            ->setDescription(\'Build config and common file cache.\');\n"
	"    }\n"
	"\n"
	"    protected function execute(Input $input, Output $output)\n"
	"    {\n"
	"        if ($input->getArgument(\'module\')) {\n"
	"            $module = $input->getArgument(\'module\') . DS;\n"
	"        } else {\n"
	"            $module = \'\';\n"
	"        }\n"
	"\n"
	"        $content = \'<?php \' . PHP_EOL . $this->buildCacheContent($module);\n"
	"\n"
	"        if (!is_dir(RUNTIME_PATH . $module)) {\n"
	"            @mkdir(RUNTIME_PATH . $module, 0755, true);\n"
	"        }\n"
	"\n"
	"        file_put_contents(RUNTIME_PATH . $module . \'init\' . EXT, $content);\n"
	"\n"
	"        $output->writeln(\'<info>Succeed!</info>\');\n"
	"    }\n"
	"\n"
	"    protected function buildCacheContent($module)\n"
	"    {\n"
	"        $content = \'\';\n"
	"        $path    = realpath(APP_PATH . $module) . DS;\n"
	"\n"
	"        if ($module) {\n"
	"            // 加载模块配置\n"
	"            $config = ThinkConfig::load(CONF_PATH . $module . \'config\' . CONF_EXT);\n"
	"\n"
	"            // 读取数据库配置文件\n"
	"            $filename = CONF_PATH . $module . \'database\' . CONF_EXT;\n"
	"            ThinkConfig::load($filename, \'database\');\n"
	"\n"
	"            // 加载应用状态配置\n"
	"            if ($config[\'app_status\']) {\n"
	"                $config = ThinkConfig::load(CONF_PATH . $module . $config[\'app_status\'] . CONF_EXT);\n"
	"            }\n"
	"            // 读取扩展配置文件\n"
	"            if (is_dir(CONF_PATH . $module . \'extra\')) {\n"
	"                $dir   = CONF_PATH . $module . \'extra\';\n"
	"                $files = scandir($dir);\n"
	"                foreach ($files as $file) {\n"
	"                    if (strpos($file, CONF_EXT)) {\n"
	"                        $filename = $dir . DS . $file;\n"
	"                        ThinkConfig::load($filename, pathinfo($file, PATHINFO_FILENAME));\n"
	"                    }\n"
	"                }\n"
	"            }\n"
	"        }\n"
	"\n"
	"        // 加载行为扩展文件\n"
	"        if (is_file(CONF_PATH . $module . \'tags\' . EXT)) {\n"
	"            $content .= \'\\think\\Hook::import(\' . (var_export(include CONF_PATH . $module . \'tags\' . EXT, true)) . \');\' . PHP_EOL;\n"
	"        }\n"
	"\n"
	"        // 加载公共文件\n"
	"        if (is_file($path . \'common\' . EXT)) {\n"
	"            $content .= substr(php_strip_whitespace($path . \'common\' . EXT), 5) . PHP_EOL;\n"
	"        }\n"
	"\n"
	"        $content .= \'\\think\\Config::set(\' . var_export(ThinkConfig::get(), true) . \');\';\n"
	"        return $content;\n"
	"    }\n"
	"}\n"
	"\n"
;
#endif