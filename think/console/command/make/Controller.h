#ifndef THINK_THINK_CONSOLE_COMMAND_MAKE_CONTROLLER_H
#define THINK_THINK_CONSOLE_COMMAND_MAKE_CONTROLLER_H

static char* think_console_command_make_controller =
	"\n"
	"// +----------------------------------------------------------------------\n"
	"// | ThinkPHP [ WE CAN DO IT JUST THINK ]\n"
	"// +----------------------------------------------------------------------\n"
	"// | Copyright (c) 2016 http://thinkphp.cn All rights reserved.\n"
	"// +----------------------------------------------------------------------\n"
	"// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )\n"
	"// +----------------------------------------------------------------------\n"
	"// | Author: 刘志淳 <chun@engineer.com>\n"
	"// +----------------------------------------------------------------------\n"
	"\n"
	"namespace think\\console\\command\\make;\n"
	"\n"
	"use think\\Config;\n"
	"use think\\console\\command\\Make;\n"
	"use think\\console\\input\\Option;\n"
	"\n"
	"class Controller extends Make\n"
	"{\n"
	"\n"
	"    protected $type = \"Controller\";\n"
	"\n"
	"    protected function configure()\n"
	"    {\n"
	"        parent::configure();\n"
	"        $this->setName(\'make:controller\')\n"
	"            ->addOption(\'plain\', null, Option::VALUE_NONE, \'Generate an empty controller class.\')\n"
	"            ->setDescription(\'Create a new resource controller class\');\n"
	"    }\n"
	"\n"
	"    protected function getStub()\n"
	"    {\n"
	"        if ($this->input->getOption(\'plain\')) {\n"
	"            return __DIR__ . \'/stubs/controller.plain.stub\';\n"
	"        }\n"
	"\n"
	"        return __DIR__ . \'/stubs/controller.stub\';\n"
	"    }\n"
	"\n"
	"    protected function getClassName($name)\n"
	"    {\n"
	"        return parent::getClassName($name) . (Config::get(\'controller_suffix\') ? ucfirst(Config::get(\'url_controller_layer\')) : \'\');\n"
	"    }\n"
	"\n"
	"    protected function getNamespace($appNamespace, $module)\n"
	"    {\n"
	"        return parent::getNamespace($appNamespace, $module) . \'\\controller\';\n"
	"    }\n"
	"\n"
	"}\n"
	"\n"
;
#endif