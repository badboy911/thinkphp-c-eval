#ifndef THINK_THINK_EXCEPTION_PDOEXCEPTION_H
#define THINK_THINK_EXCEPTION_PDOEXCEPTION_H

static char* think_exception_pdoexception =
	"\n"
	"// +----------------------------------------------------------------------\n"
	"// | ThinkPHP [ WE CAN DO IT JUST THINK ]\n"
	"// +----------------------------------------------------------------------\n"
	"// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.\n"
	"// +----------------------------------------------------------------------\n"
	"// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )\n"
	"// +----------------------------------------------------------------------\n"
	"// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://zjzit.cn>\n"
	"// +----------------------------------------------------------------------\n"
	"\n"
	"namespace think\\exception;\n"
	"\n"
	"/**\n"
	" * PDO异常处理类\n"
	" * 重新封装了系统的\\PDOException类\n"
	" */\n"
	"class PDOException extends DbException\n"
	"{\n"
	"    /**\n"
	"     * PDOException constructor.\n"
	"     * @param \\PDOException $exception\n"
	"     * @param array         $config\n"
	"     * @param string        $sql\n"
	"     * @param int           $code\n"
	"     */\n"
	"    public function __construct(\\PDOException $exception, array $config, $sql, $code = 10501)\n"
	"    {\n"
	"        $error = $exception->errorInfo;\n"
	"\n"
	"        $this->setData(\'PDO Error Info\', [\n"
	"            \'SQLSTATE\'             => $error[0],\n"
	"            \'Driver Error Code\'    => isset($error[1]) ? $error[1] : 0,\n"
	"            \'Driver Error Message\' => isset($error[2]) ? $error[2] : \'\',\n"
	"        ]);\n"
	"\n"
	"        parent::__construct($exception->getMessage(), $config, $sql, $code);\n"
	"    }\n"
	"}\n"
	"\n"
;
#endif