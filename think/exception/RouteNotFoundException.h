#ifndef THINK_THINK_EXCEPTION_ROUTENOTFOUNDEXCEPTION_H
#define THINK_THINK_EXCEPTION_ROUTENOTFOUNDEXCEPTION_H

static char* think_exception_routenotfoundexception =
	"\n"
	"// +----------------------------------------------------------------------\n"
	"// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]\n"
	"// +----------------------------------------------------------------------\n"
	"// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.\n"
	"// +----------------------------------------------------------------------\n"
	"// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )\n"
	"// +----------------------------------------------------------------------\n"
	"// | Author: yunwuxin <448901948@qq.com>\n"
	"// +----------------------------------------------------------------------\n"
	"\n"
	"namespace think\\exception;\n"
	"\n"
	"class RouteNotFoundException extends HttpException\n"
	"{\n"
	"\n"
	"    public function __construct()\n"
	"    {\n"
	"        parent::__construct(404, \'Route Not Found\');\n"
	"    }\n"
	"\n"
	"}\n"
	"\n"
;
#endif