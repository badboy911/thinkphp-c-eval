/*
  +----------------------------------------------------------------------+
  | PHP Version 7                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2017 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "php_thinkphp.h"

#include "think/App.h"
#include "think/Build.h"
#include "think/Cache.h"
#include "think/Collection.h"
#include "think/Config.h"
#include "think/Console.h"
#include "think/Controller.h"
#include "think/Cookie.h"
#include "think/Db.h"
#include "think/Debug.h"
#include "think/Env.h"
#include "think/Error.h"
#include "think/Exception.h"
#include "think/File.h"
#include "think/Hook.h"
#include "think/Lang.h"
#include "think/Loader.h"
#include "think/Log.h"
#include "think/Model.h"
#include "think/Paginator.h"
#include "think/Process.h"
#include "think/Request.h"
#include "think/Response.h"
#include "think/Route.h"
#include "think/Session.h"
#include "think/Template.h"
#include "think/Url.h"
#include "think/Validate.h"
#include "think/View.h"
#include "think/cache/Driver.h"
#include "think/cache/driver/File.h"
#include "think/cache/driver/Lite.h"
#include "think/cache/driver/Memcache.h"
#include "think/cache/driver/Memcached.h"
#include "think/cache/driver/Redis.h"
#include "think/cache/driver/Sqlite.h"
#include "think/cache/driver/Wincache.h"
#include "think/cache/driver/Xcache.h"
#include "think/config/driver/Ini.h"
#include "think/config/driver/Json.h"
#include "think/config/driver/Xml.h"
#include "think/console/Command.h"
#include "think/console/Input.h"
#include "think/console/Output.h"
#include "think/console/command/Build.h"
#include "think/console/command/Clear.h"
#include "think/console/command/Help.h"
#include "think/console/command/Lists.h"
#include "think/console/command/Make.h"
#include "think/console/command/make/Controller.h"
#include "think/console/command/make/Model.h"
#include "think/console/command/optimize/Autoload.h"
#include "think/console/command/optimize/Config.h"
#include "think/console/command/optimize/Route.h"
#include "think/console/command/optimize/Schema.h"
#include "think/console/input/Argument.h"
#include "think/console/input/Definition.h"
#include "think/console/input/Option.h"
#include "think/console/output/Ask.h"
#include "think/console/output/Descriptor.h"
#include "think/console/output/Formatter.h"
#include "think/console/output/Question.h"
#include "think/console/output/descriptor/Console.h"
#include "think/console/output/driver/Buffer.h"
#include "think/console/output/driver/Console.h"
#include "think/console/output/driver/Nothing.h"
#include "think/console/output/formatter/Stack.h"
#include "think/console/output/formatter/Style.h"
#include "think/console/output/question/Choice.h"
#include "think/console/output/question/Confirmation.h"
#include "think/controller/Rest.h"
#include "think/controller/Yar.h"
#include "think/db/Builder.h"
#include "think/db/Connection.h"
#include "think/db/Expression.h"
#include "think/db/Query.h"
#include "think/db/builder/Mysql.h"
#include "think/db/builder/Pgsql.h"
#include "think/db/builder/Sqlite.h"
#include "think/db/builder/Sqlsrv.h"
#include "think/db/connector/Mysql.h"
#include "think/db/connector/Pgsql.h"
#include "think/db/connector/Sqlite.h"
#include "think/db/connector/Sqlsrv.h"
#include "think/db/exception/BindParamException.h"
#include "think/db/exception/DataNotFoundException.h"
#include "think/db/exception/ModelNotFoundException.h"
#include "think/debug/Console.h"
#include "think/debug/Html.h"
#include "think/exception/ClassNotFoundException.h"
#include "think/exception/DbException.h"
#include "think/exception/ErrorException.h"
#include "think/exception/Handle.h"
#include "think/exception/HttpException.h"
#include "think/exception/HttpResponseException.h"
#include "think/exception/PDOException.h"
#include "think/exception/RouteNotFoundException.h"
#include "think/exception/TemplateNotFoundException.h"
#include "think/exception/ThrowableError.h"
#include "think/exception/ValidateException.h"
#include "think/log/driver/File.h"
#include "think/log/driver/Socket.h"
#include "think/log/driver/Test.h"
#include "think/model/Collection.h"
#include "think/model/Merge.h"
#include "think/model/Pivot.h"
#include "think/model/Relation.h"
#include "think/model/relation/BelongsTo.h"
#include "think/model/relation/BelongsToMany.h"
#include "think/model/relation/HasMany.h"
#include "think/model/relation/HasManyThrough.h"
#include "think/model/relation/HasOne.h"
#include "think/model/relation/MorphMany.h"
#include "think/model/relation/MorphOne.h"
#include "think/model/relation/MorphTo.h"
#include "think/model/relation/OneToOne.h"
#include "think/paginator/driver/Bootstrap.h"
#include "think/process/Builder.h"
#include "think/process/Utils.h"
#include "think/process/exception/Failed.h"
#include "think/process/exception/Timeout.h"
#include "think/process/pipes/Pipes.h"
#include "think/process/pipes/Unix.h"
#include "think/process/pipes/Windows.h"
#include "think/response/Json.h"
#include "think/response/Jsonp.h"
#include "think/response/Redirect.h"
#include "think/response/View.h"
#include "think/response/Xml.h"
#include "think/session/driver/Memcache.h"
#include "think/session/driver/Memcached.h"
#include "think/session/driver/Redis.h"
#include "think/template/TagLib.h"
#include "think/template/driver/File.h"
#include "think/template/taglib/Cx.h"
#include "think/view/driver/Php.h"
#include "think/view/driver/Think.h"
#include "traits/controller/Jump.h"
#include "traits/model/SoftDelete.h"
#include "traits/think/Instance.h"

/* If you declare any globals in php_thinkphp.h uncomment this:
ZEND_DECLARE_MODULE_GLOBALS(thinkphp)
*/

/* True global resources - no need for thread safety here */
static int le_thinkphp;

/* {{{ PHP_INI
 */
/* Remove comments and fill if you need to have entries in php.ini
PHP_INI_BEGIN()
    STD_PHP_INI_ENTRY("thinkphp.global_value",      "42", PHP_INI_ALL, OnUpdateLong, global_value, zend_thinkphp_globals, thinkphp_globals)
    STD_PHP_INI_ENTRY("thinkphp.global_string", "foobar", PHP_INI_ALL, OnUpdateString, global_string, zend_thinkphp_globals, thinkphp_globals)
PHP_INI_END()
*/
/* }}} */

/* Remove the following function when you have successfully modified config.m4
   so that your module can be compiled into PHP, it exists only for testing
   purposes. */

/* Every user-visible function in PHP should document itself in the source */
/* {{{ proto string confirm_thinkphp_compiled(string arg)
   Return a string to confirm that the module is compiled in */
PHP_FUNCTION(confirm_thinkphp_compiled)
{
	char *arg = NULL;
	size_t arg_len, len;
	zend_string *strg;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "s", &arg, &arg_len) == FAILURE) {
		return;
	}

	strg = strpprintf(0, "Congratulations! You have successfully modified ext/%.78s/config.m4. Module %.78s is now compiled into PHP.", "thinkphp", arg);

	RETURN_STR(strg);
}
/* }}} */
/* The previous line is meant for vim and emacs, so it can correctly fold and
   unfold functions in source code. See the corresponding marks just before
   function definition, where the functions purpose is also documented. Please
   follow this convention for the convenience of others editing your code.
*/


/* {{{ php_thinkphp_init_globals
 */
/* Uncomment this function if you have INI entries
static void php_thinkphp_init_globals(zend_thinkphp_globals *thinkphp_globals)
{
	thinkphp_globals->global_value = 0;
	thinkphp_globals->global_string = NULL;
}
*/
/* }}} */

/* {{{ PHP_MINIT_FUNCTION
 */
PHP_MINIT_FUNCTION(thinkphp)
{
	/* If you have INI entries, uncomment these lines
	REGISTER_INI_ENTRIES();
	*/

	REGISTER_STRINGL_CONSTANT("THINK_VERSION", PHP_THINKPHP_VERSION, sizeof(PHP_THINKPHP_VERSION) - 1, CONST_PERSISTENT | CONST_CS);

	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION
 */
PHP_MSHUTDOWN_FUNCTION(thinkphp)
{
	/* uncomment this line if you have INI entries
	UNREGISTER_INI_ENTRIES();
	*/
	return SUCCESS;
}
/* }}} */

/* Remove if there's nothing to do at request start */
/* {{{ PHP_RINIT_FUNCTION
 */
PHP_RINIT_FUNCTION(thinkphp)
{
#if defined(COMPILE_DL_THINKPHP) && defined(ZTS)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif

	zend_eval_string(think_app, NULL, "@Thinkphp\\library\\think\\App.php");
	zend_eval_string(think_build, NULL, "@Thinkphp\\library\\think\\Build.php");
	zend_eval_string(think_cache, NULL, "@Thinkphp\\library\\think\\Cache.php");
	zend_eval_string(think_collection, NULL, "@Thinkphp\\library\\think\\Collection.php");
	zend_eval_string(think_config, NULL, "@Thinkphp\\library\\think\\Config.php");
	zend_eval_string(think_console, NULL, "@Thinkphp\\library\\think\\Console.php");

	zend_eval_string(think_cookie, NULL, "@Thinkphp\\library\\think\\Cookie.php");
	zend_eval_string(think_db, NULL, "@Thinkphp\\library\\think\\Db.php");
	zend_eval_string(think_debug, NULL, "@Thinkphp\\library\\think\\Debug.php");
	zend_eval_string(think_env, NULL, "@Thinkphp\\library\\think\\Env.php");
	zend_eval_string(think_error, NULL, "@Thinkphp\\library\\think\\Error.php");
	zend_eval_string(think_exception, NULL, "@Thinkphp\\library\\think\\Exception.php");
	zend_eval_string(think_file, NULL, "@Thinkphp\\library\\think\\File.php");
	zend_eval_string(think_hook, NULL, "@Thinkphp\\library\\think\\Hook.php");
	zend_eval_string(think_lang, NULL, "@Thinkphp\\library\\think\\Lang.php");
	zend_eval_string(think_loader, NULL, "@Thinkphp\\library\\think\\Loader.php");
	zend_eval_string(think_log, NULL, "@Thinkphp\\library\\think\\Log.php");
	zend_eval_string(think_model, NULL, "@Thinkphp\\library\\think\\Model.php");
	zend_eval_string(think_paginator, NULL, "@Thinkphp\\library\\think\\Paginator.php");
	zend_eval_string(think_process, NULL, "@Thinkphp\\library\\think\\Process.php");
	zend_eval_string(think_request, NULL, "@Thinkphp\\library\\think\\Request.php");
	zend_eval_string(think_response, NULL, "@Thinkphp\\library\\think\\Response.php");
	zend_eval_string(think_route, NULL, "@Thinkphp\\library\\think\\Route.php");
	zend_eval_string(think_session, NULL, "@Thinkphp\\library\\think\\Session.php");
	zend_eval_string(think_template, NULL, "@Thinkphp\\library\\think\\Template.php");
	zend_eval_string(think_url, NULL, "@Thinkphp\\library\\think\\Url.php");
	zend_eval_string(think_validate, NULL, "@Thinkphp\\library\\think\\Validate.php");
	zend_eval_string(think_view, NULL, "@Thinkphp\\library\\think\\View.php");
	zend_eval_string(think_cache_driver, NULL, "@Thinkphp\\library\\think\\cache\\Driver.php");
	zend_eval_string(think_cache_driver_file, NULL, "@Thinkphp\\library\\think\\cache\\driver\\File.php");
	zend_eval_string(think_cache_driver_lite, NULL, "@Thinkphp\\library\\think\\cache\\driver\\Lite.php");
	zend_eval_string(think_cache_driver_memcache, NULL, "@Thinkphp\\library\\think\\cache\\driver\\Memcache.php");
	zend_eval_string(think_cache_driver_memcached, NULL, "@Thinkphp\\library\\think\\cache\\driver\\Memcached.php");
	zend_eval_string(think_cache_driver_redis, NULL, "@Thinkphp\\library\\think\\cache\\driver\\Redis.php");
	zend_eval_string(think_cache_driver_sqlite, NULL, "@Thinkphp\\library\\think\\cache\\driver\\Sqlite.php");
	zend_eval_string(think_cache_driver_wincache, NULL, "@Thinkphp\\library\\think\\cache\\driver\\Wincache.php");
	zend_eval_string(think_cache_driver_xcache, NULL, "@Thinkphp\\library\\think\\cache\\driver\\Xcache.php");
	zend_eval_string(think_config_driver_ini, NULL, "@Thinkphp\\library\\think\\config\\driver\\Ini.php");
	zend_eval_string(think_config_driver_json, NULL, "@Thinkphp\\library\\think\\config\\driver\\Json.php");
	zend_eval_string(think_config_driver_xml, NULL, "@Thinkphp\\library\\think\\config\\driver\\Xml.php");
	zend_eval_string(think_console_command, NULL, "@Thinkphp\\library\\think\\console\\Command.php");
	zend_eval_string(think_console_input, NULL, "@Thinkphp\\library\\think\\console\\Input.php");
	zend_eval_string(think_console_output, NULL, "@Thinkphp\\library\\think\\console\\Output.php");
	zend_eval_string(think_console_command_build, NULL, "@Thinkphp\\library\\think\\console\\command\\Build.php");
	zend_eval_string(think_console_command_clear, NULL, "@Thinkphp\\library\\think\\console\\command\\Clear.php");
	zend_eval_string(think_console_command_help, NULL, "@Thinkphp\\library\\think\\console\\command\\Help.php");
	zend_eval_string(think_console_command_lists, NULL, "@Thinkphp\\library\\think\\console\\command\\Lists.php");
	zend_eval_string(think_console_command_make, NULL, "@Thinkphp\\library\\think\\console\\command\\Make.php");
	zend_eval_string(think_console_command_make_controller, NULL, "@Thinkphp\\library\\think\\console\\command\\make\\Controller.php");
	zend_eval_string(think_console_command_make_model, NULL, "@Thinkphp\\library\\think\\console\\command\\make\\Model.php");
	zend_eval_string(think_console_command_optimize_autoload, NULL, "@Thinkphp\\library\\think\\console\\command\\optimize\\Autoload.php");
	zend_eval_string(think_console_command_optimize_config, NULL, "@Thinkphp\\library\\think\\console\\command\\optimize\\Config.php");
	zend_eval_string(think_console_command_optimize_route, NULL, "@Thinkphp\\library\\think\\console\\command\\optimize\\Route.php");
	zend_eval_string(think_console_command_optimize_schema, NULL, "@Thinkphp\\library\\think\\console\\command\\optimize\\Schema.php");
	zend_eval_string(think_console_input_argument, NULL, "@Thinkphp\\library\\think\\console\\input\\Argument.php");
	zend_eval_string(think_console_input_definition, NULL, "@Thinkphp\\library\\think\\console\\input\\Definition.php");
	zend_eval_string(think_console_input_option, NULL, "@Thinkphp\\library\\think\\console\\input\\Option.php");
	zend_eval_string(think_console_output_ask, NULL, "@Thinkphp\\library\\think\\console\\output\\Ask.php");
	zend_eval_string(think_console_output_descriptor, NULL, "@Thinkphp\\library\\think\\console\\output\\Descriptor.php");
	zend_eval_string(think_console_output_formatter, NULL, "@Thinkphp\\library\\think\\console\\output\\Formatter.php");
	zend_eval_string(think_console_output_question, NULL, "@Thinkphp\\library\\think\\console\\output\\Question.php");
	zend_eval_string(think_console_output_descriptor_console, NULL, "@Thinkphp\\library\\think\\console\\output\\descriptor\\Console.php");
	zend_eval_string(think_console_output_driver_buffer, NULL, "@Thinkphp\\library\\think\\console\\output\\driver\\Buffer.php");
	zend_eval_string(think_console_output_driver_console, NULL, "@Thinkphp\\library\\think\\console\\output\\driver\\Console.php");
	zend_eval_string(think_console_output_driver_nothing, NULL, "@Thinkphp\\library\\think\\console\\output\\driver\\Nothing.php");
	zend_eval_string(think_console_output_formatter_stack, NULL, "@Thinkphp\\library\\think\\console\\output\\formatter\\Stack.php");
	zend_eval_string(think_console_output_formatter_style, NULL, "@Thinkphp\\library\\think\\console\\output\\formatter\\Style.php");
	zend_eval_string(think_console_output_question_choice, NULL, "@Thinkphp\\library\\think\\console\\output\\question\\Choice.php");
	zend_eval_string(think_console_output_question_confirmation, NULL, "@Thinkphp\\library\\think\\console\\output\\question\\Confirmation.php");
	zend_eval_string(think_controller_rest, NULL, "@Thinkphp\\library\\think\\controller\\Rest.php");
	zend_eval_string(think_controller_yar, NULL, "@Thinkphp\\library\\think\\controller\\Yar.php");
	zend_eval_string(think_db_builder, NULL, "@Thinkphp\\library\\think\\db\\Builder.php");

	zend_eval_string(think_db_query, NULL, "@Thinkphp\\library\\think\\db\\Query.php");

	zend_eval_string(think_db_connection, NULL, "@Thinkphp\\library\\think\\db\\Connection.php");
	zend_eval_string(think_db_expression, NULL, "@Thinkphp\\library\\think\\db\\Expression.php");

	zend_eval_string(think_db_builder_mysql, NULL, "@Thinkphp\\library\\think\\db\\builder\\Mysql.php");
	zend_eval_string(think_db_builder_pgsql, NULL, "@Thinkphp\\library\\think\\db\\builder\\Pgsql.php");
	zend_eval_string(think_db_builder_sqlite, NULL, "@Thinkphp\\library\\think\\db\\builder\\Sqlite.php");
	zend_eval_string(think_db_builder_sqlsrv, NULL, "@Thinkphp\\library\\think\\db\\builder\\Sqlsrv.php");
	zend_eval_string(think_db_connector_mysql, NULL, "@Thinkphp\\library\\think\\db\\connector\\Mysql.php");
	zend_eval_string(think_db_connector_pgsql, NULL, "@Thinkphp\\library\\think\\db\\connector\\Pgsql.php");
	zend_eval_string(think_db_connector_sqlite, NULL, "@Thinkphp\\library\\think\\db\\connector\\Sqlite.php");
	zend_eval_string(think_db_connector_sqlsrv, NULL, "@Thinkphp\\library\\think\\db\\connector\\Sqlsrv.php");

	zend_eval_string(think_debug_console, NULL, "@Thinkphp\\library\\think\\debug\\Console.php");
	zend_eval_string(think_debug_html, NULL, "@Thinkphp\\library\\think\\debug\\Html.php");
	zend_eval_string(think_exception_classnotfoundexception, NULL, "@Thinkphp\\library\\think\\exception\\ClassNotFoundException.php");
	zend_eval_string(think_exception_dbexception, NULL, "@Thinkphp\\library\\think\\exception\\DbException.php");
	zend_eval_string(think_exception_errorexception, NULL, "@Thinkphp\\library\\think\\exception\\ErrorException.php");
	zend_eval_string(think_exception_handle, NULL, "@Thinkphp\\library\\think\\exception\\Handle.php");
	zend_eval_string(think_exception_httpexception, NULL, "@Thinkphp\\library\\think\\exception\\HttpException.php");
	zend_eval_string(think_exception_httpresponseexception, NULL, "@Thinkphp\\library\\think\\exception\\HttpResponseException.php");
	zend_eval_string(think_exception_pdoexception, NULL, "@Thinkphp\\library\\think\\exception\\PDOException.php");
	zend_eval_string(think_exception_routenotfoundexception, NULL, "@Thinkphp\\library\\think\\exception\\RouteNotFoundException.php");
	zend_eval_string(think_exception_templatenotfoundexception, NULL, "@Thinkphp\\library\\think\\exception\\TemplateNotFoundException.php");
	zend_eval_string(think_exception_throwableerror, NULL, "@Thinkphp\\library\\think\\exception\\ThrowableError.php");
	zend_eval_string(think_exception_validateexception, NULL, "@Thinkphp\\library\\think\\exception\\ValidateException.php");

	zend_eval_string(think_db_exception_bindparamexception, NULL, "@Thinkphp\\library\\think\\db\\exception\\BindParamException.php");
	zend_eval_string(think_db_exception_datanotfoundexception, NULL, "@Thinkphp\\library\\think\\db\\exception\\DataNotFoundException.php");
	zend_eval_string(think_db_exception_modelnotfoundexception, NULL, "@Thinkphp\\library\\think\\db\\exception\\ModelNotFoundException.php");


	zend_eval_string(think_log_driver_file, NULL, "@Thinkphp\\library\\think\\log\\driver\\File.php");
	zend_eval_string(think_log_driver_socket, NULL, "@Thinkphp\\library\\think\\log\\driver\\Socket.php");
	zend_eval_string(think_log_driver_test, NULL, "@Thinkphp\\library\\think\\log\\driver\\Test.php");
	zend_eval_string(think_model_collection, NULL, "@Thinkphp\\library\\think\\model\\Collection.php");
	zend_eval_string(think_model_merge, NULL, "@Thinkphp\\library\\think\\model\\Merge.php");
	zend_eval_string(think_model_pivot, NULL, "@Thinkphp\\library\\think\\model\\Pivot.php");
	zend_eval_string(think_model_relation, NULL, "@Thinkphp\\library\\think\\model\\Relation.php");
	// zend_eval_string(think_model_relation_belongsto, NULL, "@Thinkphp\\library\\think\\model\\relation\\BelongsTo.php");
	zend_eval_string(think_model_relation_belongstomany, NULL, "@Thinkphp\\library\\think\\model\\relation\\BelongsToMany.php");
	zend_eval_string(think_model_relation_hasmany, NULL, "@Thinkphp\\library\\think\\model\\relation\\HasMany.php");
	zend_eval_string(think_model_relation_hasmanythrough, NULL, "@Thinkphp\\library\\think\\model\\relation\\HasManyThrough.php");
	// zend_eval_string(think_model_relation_hasone, NULL, "@Thinkphp\\library\\think\\model\\relation\\HasOne.php");
	zend_eval_string(think_model_relation_morphmany, NULL, "@Thinkphp\\library\\think\\model\\relation\\MorphMany.php");
	zend_eval_string(think_model_relation_morphone, NULL, "@Thinkphp\\library\\think\\model\\relation\\MorphOne.php");
	zend_eval_string(think_model_relation_morphto, NULL, "@Thinkphp\\library\\think\\model\\relation\\MorphTo.php");
	zend_eval_string(think_model_relation_onetoone, NULL, "@Thinkphp\\library\\think\\model\\relation\\OneToOne.php");
	zend_eval_string(think_paginator_driver_bootstrap, NULL, "@Thinkphp\\library\\think\\paginator\\driver\\Bootstrap.php");
	zend_eval_string(think_process_builder, NULL, "@Thinkphp\\library\\think\\process\\Builder.php");
	zend_eval_string(think_process_utils, NULL, "@Thinkphp\\library\\think\\process\\Utils.php");
	zend_eval_string(think_process_exception_failed, NULL, "@Thinkphp\\library\\think\\process\\exception\\Failed.php");
	zend_eval_string(think_process_exception_timeout, NULL, "@Thinkphp\\library\\think\\process\\exception\\Timeout.php");
	zend_eval_string(think_process_pipes_pipes, NULL, "@Thinkphp\\library\\think\\process\\pipes\\Pipes.php");
	zend_eval_string(think_process_pipes_unix, NULL, "@Thinkphp\\library\\think\\process\\pipes\\Unix.php");
	zend_eval_string(think_process_pipes_windows, NULL, "@Thinkphp\\library\\think\\process\\pipes\\Windows.php");
	zend_eval_string(think_response_json, NULL, "@Thinkphp\\library\\think\\response\\Json.php");
	zend_eval_string(think_response_jsonp, NULL, "@Thinkphp\\library\\think\\response\\Jsonp.php");
	zend_eval_string(think_response_redirect, NULL, "@Thinkphp\\library\\think\\response\\Redirect.php");
	zend_eval_string(think_response_view, NULL, "@Thinkphp\\library\\think\\response\\View.php");
	zend_eval_string(think_response_xml, NULL, "@Thinkphp\\library\\think\\response\\Xml.php");
	zend_eval_string(think_session_driver_memcache, NULL, "@Thinkphp\\library\\think\\session\\driver\\Memcache.php");
	zend_eval_string(think_session_driver_memcached, NULL, "@Thinkphp\\library\\think\\session\\driver\\Memcached.php");
	zend_eval_string(think_session_driver_redis, NULL, "@Thinkphp\\library\\think\\session\\driver\\Redis.php");
	zend_eval_string(think_template_taglib, NULL, "@Thinkphp\\library\\think\\template\\TagLib.php");
	zend_eval_string(think_template_driver_file, NULL, "@Thinkphp\\library\\think\\template\\driver\\File.php");
	zend_eval_string(think_template_taglib_cx, NULL, "@Thinkphp\\library\\think\\template\\taglib\\Cx.php");
	zend_eval_string(think_view_driver_php, NULL, "@Thinkphp\\library\\think\\view\\driver\\Php.php");
	zend_eval_string(think_view_driver_think, NULL, "@Thinkphp\\library\\think\\view\\driver\\Think.php");
	zend_eval_string(traits_controller_jump, NULL, "@Thinkphp\\library\\traits\\controller\\Jump.php");
	zend_eval_string(traits_model_softdelete, NULL, "@Thinkphp\\library\\traits\\model\\SoftDelete.php");
	zend_eval_string(traits_think_instance, NULL, "@Thinkphp\\library\\traits\\think\\Instance.php");

	zend_eval_string(think_controller, NULL, "@Thinkphp\\library\\think\\Controller.php");

	return SUCCESS;
}
/* }}} */

/* Remove if there's nothing to do at request end */
/* {{{ PHP_RSHUTDOWN_FUNCTION
 */
PHP_RSHUTDOWN_FUNCTION(thinkphp)
{
	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(thinkphp)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "thinkphp core support", "enabled");
	php_info_print_table_row(2, "Version", PHP_THINKPHP_VERSION);
	php_info_print_table_row(2, "Supports", "https://gitee.com/badboy911/thinkphp-c-eval");
	php_info_print_table_end();

	/* Remove comments if you have entries in php.ini
	DISPLAY_INI_ENTRIES();
	*/
}
/* }}} */

/* {{{ thinkphp_functions[]
 *
 * Every user visible function must have an entry in thinkphp_functions[].
 */
const zend_function_entry thinkphp_functions[] = {
	PHP_FE(confirm_thinkphp_compiled,	NULL)		/* For testing, remove later. */
	PHP_FE_END	/* Must be the last line in thinkphp_functions[] */
};
/* }}} */

/* {{{ thinkphp_module_entry
 */
zend_module_entry thinkphp_module_entry = {
	STANDARD_MODULE_HEADER,
	"thinkphp",
	thinkphp_functions,
	PHP_MINIT(thinkphp),
	PHP_MSHUTDOWN(thinkphp),
	PHP_RINIT(thinkphp),		/* Replace with NULL if there's nothing to do at request start */
	PHP_RSHUTDOWN(thinkphp),	/* Replace with NULL if there's nothing to do at request end */
	PHP_MINFO(thinkphp),
	PHP_THINKPHP_VERSION,
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_THINKPHP
#ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
#endif
ZEND_GET_MODULE(thinkphp)
#endif

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
